/**
 * Created by Mehdi Mammadov on 08.06.2017.
 */

function generatePlaylistFromArray(filesList, selectedItem) {
    var ol = document.getElementById("filesInPlaylist");
    ol.innerHTML = "";

    for(var i = 0; i < filesList.length; i++){
        var listItem = document.createElement("li");

        if (selectedItem === undefined){
            listItem.className = "ui-widget-content";
        }
        else {
            if(selectedItem === i){
                listItem.className = "ui-widget-content ui-selected";
                selectedRecordInPlaylist.indexOfItem = selectedItem;
            }
            else{
                listItem.className = "ui-widget-content";
            }
        }

        var liContainer = document.createElement("div");
        liContainer.className = "li-container";

        var liFilename = document.createElement("div");
        liFilename.className = "filename";
        liFilename.appendChild(document.createTextNode(filesList[i].filename));
        liContainer.appendChild(liFilename);

        var liFileinfo = document.createElement("div");
        liFileinfo.className = "fileInfo";

        var liHBLabel = document.createElement("div");
        liHBLabel.className = "hasBitrateVariantLabel";
        liHBLabel.appendChild(document.createTextNode("available on"));
        liFileinfo.appendChild(liHBLabel);

        var liHB = document.createElement("div");
        liHB.className = "hasBitrateVariant";
        liHB.appendChild(document.createTextNode(filesList[i].bitrateVariants));
        liFileinfo.appendChild(liHB);

        var liHPLabel = document.createElement("div");
        liHPLabel.className = "hasProblemsLabel";
        liHPLabel.appendChild(document.createTextNode("problems"));
        liFileinfo.appendChild(liHPLabel);

        var liHP = document.createElement("div");
        liHP.appendChild(document.createTextNode("0"));
        liHP.className = "hasnotProblems";
        liFileinfo.appendChild(liHP);

        liContainer.appendChild(liFileinfo);
        listItem.appendChild(liContainer);
        ol.appendChild(listItem);
    }

    if (selectedItem === undefined){
        return false;
    }

    return false;
}

function proceedMessage(messageObj) {
    messageObj = messageObj.replace(/(&quot\;)/g, "\"");
    var mObj = JSON.parse(messageObj);
    if(mObj.isErrorAccured === true){
        showAlertBox(mObj.messageText);
    }
}

function addElementToPlaylist(filenameValue, filesList) {
    if (filenameValue === undefined){
        showAlertBox("Please, select file to add");
    }

    var newElemet = {};
    var dParser = new DOMParser();
    var domObject = dParser.parseFromString(filenameValue.html(), "text/html");

    var elementFromUI = domObject.getElementsByClassName("hasProblems");
    if(elementFromUI[0] === undefined){
        newElemet.hasProblems = 0;
    }
    else {
        showAlertBox("That video has problem. Only 1 quality of video. For streaming need 3 quality.");

        generatePlaylistFromArray(filesList);
        return false;
    }

    elementFromUI = domObject.getElementsByClassName("filename");
    newElemet.filename = elementFromUI[0].childNodes[0].nodeValue;

    elementFromUI = domObject.getElementsByClassName("hasBitrateVariant");
    newElemet.bitrateVariants = elementFromUI[0].childNodes[0].nodeValue;

    filesList.push(newElemet);

    generatePlaylistFromArray(filesList);

    return false;
}

function showAlertBox(messageText) {
    var checkDialog = document.getElementById("alertBox");
    if(checkDialog){ return false; }

    var modalAlertDialog = document.createElement("div");
    modalAlertDialog.setAttribute("id", "alertBox");
    var geom = {};
    geom.x = ($(window).width() - 520) / 2;
    geom.y = ($(window).height() - 240) / 2;
    geom.styleString = "left:" + geom.x + "px;top:" + geom.y + "px;";

    modalAlertDialog.setAttribute("style", geom.styleString);

    var dHeader = document.createElement("div");
    dHeader.setAttribute("id", "dialogHeader");

    var dLabel = document.createElement("div");
    dLabel.setAttribute("id", "dialogLabel");
    dLabel.appendChild(document.createTextNode("Alert"));
    dHeader.appendChild(dLabel);

    var aCloseButton = document.createElement("a");
    aCloseButton.setAttribute("href", "#");
    aCloseButton.setAttribute("onclick", "closeDialog(\"alertBox\")");

    var closeButton = document.createElement("div");
    closeButton.setAttribute("id", "closeButton");
    closeButton.appendChild(document.createTextNode("X"));
    aCloseButton.appendChild(closeButton);

    dHeader.appendChild(aCloseButton);
    modalAlertDialog.appendChild(dHeader);

    var dMessage = document.createElement("div");
    dMessage.setAttribute("id", "dialogMessage");
    dMessage.appendChild(document.createTextNode(messageText));
    modalAlertDialog.appendChild(dMessage);

    var bodyOfPage = document.getElementById("pageHeader");
    bodyOfPage.appendChild(modalAlertDialog);
}

function closeDialog(id) {
    document.getElementById(id).remove();
    if(id === "alertBox"){
        location.reload(true);
    }
}

function deleteElementFromPlaylist(recordIndex, filesList) {

    if (recordIndex.indexOfItem === undefined){
        return false;
    }

    filesList.splice(recordIndex.indexOfItem, 1);
    recordIndex.indexOfItem = undefined;


    generatePlaylistFromArray(filesList);
    return false;
}

function bringToUp(recordIndex, filesList) {
    if (recordIndex.indexOfItem === undefined){ return false; }
    if (recordIndex.indexOfItem === 0){ return false; }

    var currentValue = filesList[recordIndex.indexOfItem];
    var prevElement = {};
    prevElement.indexOfItem = recordIndex.indexOfItem - 1;
    prevElement.valueOfItem = filesList[prevElement.indexOfItem];

    filesList[prevElement.indexOfItem] = currentValue;
    filesList[recordIndex.indexOfItem] = prevElement.valueOfItem;

    generatePlaylistFromArray(filesList, prevElement.indexOfItem);
    return false;
}

function bringToDown(recordIndex, filesList) {
    if (recordIndex.indexOfItem === undefined){ return false; }
    if (recordIndex.indexOfItem === filesList.length - 1) { return false; }

    var currentValue = filesList[recordIndex.indexOfItem];
    var nextElement = {}
    nextElement.indexOfItem = recordIndex.indexOfItem + 1;
    nextElement.valueOfItem = filesList[nextElement.indexOfItem];

    filesList[nextElement.indexOfItem] = currentValue;
    filesList[recordIndex.indexOfItem] = nextElement.valueOfItem;

    generatePlaylistFromArray(filesList, nextElement.indexOfItem);
    return false;
}