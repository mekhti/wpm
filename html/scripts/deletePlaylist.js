/**
 * Created by Mehdi Mammadov on 14.06.2017.
 */

var dComboField = {};
var dDeleteButton = {};

function openDeleteDialog(playlistsJSONtext) {
    showDeleteDialog(playlistsJSONtext);
    return false;
}

function showDeleteDialog(playlistsJSONtext) {
    var dID = "deletePlaylistDialog";
    var checkDialog = document.getElementById(dID);
    if(checkDialog){ return false; }

    var modalDeleteDialog = document.createElement("div");
    modalDeleteDialog.setAttribute("id", dID);
    var geom = {};
    geom.x = ($(window).width() - 520) / 2;
    geom.y = ($(window).height() - 240) / 2;
    geom.styleString = "left:" + geom.x + "px;top:" + geom.y + "px;";

    modalDeleteDialog.setAttribute("style", geom.styleString);

    var dHeader = document.createElement("div");
    dHeader.setAttribute("id", "ddHeader");

    var dLabel = document.createElement("div");
    dLabel.setAttribute("id", "ddLabel");
    dLabel.appendChild(document.createTextNode("Delete playlist"));
    dHeader.appendChild(dLabel);

    // Dialog body
    var dBodyDiv = document.createElement("div");
    dBodyDiv.setAttribute("id", "ddBody");

    var dComboLabel = document.createElement("div");
    dComboLabel.setAttribute("id", "ddComboLabel");
    dComboLabel.appendChild(document.createTextNode("select playlist"));
    dBodyDiv.appendChild(dComboLabel);

    var dComboFieldDiv = document.createElement("div");
    dComboFieldDiv.setAttribute("id", "ddComboFieldDiv");

    dComboField = document.createElement("select");
    dComboField.setAttribute("id", "ddComboField");

    // TODO:Разобратся с этим !!!
    //dComboField.setAttribute("onchange", "onSelectedElementChanged()");

    var defaultOption = document.createElement("option");
    defaultOption.value = undefined;
    defaultOption.setAttribute("selected", "selected");

    if(playlistsJSONtext !== undefined){
        defaultOption.text = "Select playlist ...";
        dComboField.appendChild(defaultOption);
        playlistsJSONtext = playlistsJSONtext.replace(/(&quot\;)/g, "\"");
        var optionObjects = JSON.parse(playlistsJSONtext);

        optionObjects.sort(function (a, b) {
            if(a.id < b.id)
                return -1;
            else if(a.id > b.id)
                return 1;
            else 0;
        });

        for(var oCounter = 0; oCounter < optionObjects.length; oCounter++){
            var newOption = document.createElement("option");
            newOption.value = optionObjects[oCounter].id;
            newOption.text = optionObjects[oCounter].id + ".smil";
            dComboField.appendChild(newOption);
        }
    }
    else {
        defaultOption.text = "Server is empty...";
        dComboField.appendChild(defaultOption);
        dComboField.disabled = true;
    }

    dComboFieldDiv.appendChild(dComboField);
    dBodyDiv.appendChild(dComboFieldDiv);


    // Dialog button bar
    var dButtonBar = document.createElement("div");
    dButtonBar.setAttribute("id", "ddButtonBar");

    var documentUri = window.location.pathname;
    var aDeleteButton = document.createElement("a");
    aDeleteButton.setAttribute("href", window.location);
    aDeleteButton.setAttribute("onclick", ("deletePlaylist()"));

    dDeleteButton = document.createElement("div");
    dDeleteButton.setAttribute("id", "ddDeleteButton");

    /* TODO: Продолжение разборки !!!
    if(dComboField.disabled === true){
        dDeleteButton.className = "disabledButton";
    }
    else {
        dDeleteButton.className = "enabledButton";
    }
    */

    dDeleteButton.appendChild(document.createTextNode("DELETE"));

    aDeleteButton.appendChild(dDeleteButton);

    dButtonBar.appendChild(aDeleteButton);

    var aCancelButton = document.createElement("a");
    aCancelButton.setAttribute("href", "#");
    aCancelButton.setAttribute("onclick", ("closeDialog(\"" + dID + "\")"));

    var dCancelButton = document.createElement("div");
    dCancelButton.setAttribute("id", "ddCancelButton");
    dCancelButton.appendChild(document.createTextNode("CANCEL"));
    aCancelButton.appendChild(dCancelButton);

    dButtonBar.appendChild(aCancelButton);

    modalDeleteDialog.appendChild(dHeader);
    modalDeleteDialog.appendChild(dBodyDiv);
    modalDeleteDialog.appendChild(dButtonBar);

    var bodyOfPage = document.getElementById("pageHeader");
    bodyOfPage.appendChild(modalDeleteDialog);
}

function deletePlaylist() {
    var delPlaylistFilename = dComboField.options[dComboField.selectedIndex].value;
    var documentUri = window.location.pathname;

    var dID = "deletePlaylistDialog";

    $.ajax({
        url: documentUri + '/del',
        data: {
            "delPlaylistFilename" : delPlaylistFilename
        },
        cache: false,
        type: "GET"
    })

    closeDialog(dID);
}

function onSelectedElementChanged() {
    if(dComboField.options[dComboField.selectedIndex].value === "undefined"){
        dDeleteButton.className = "disabledButton";
    }
    else {
        dDeleteButton.className = "enabledButton";
    }

}