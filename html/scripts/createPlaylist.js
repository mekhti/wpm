/**
 * Created by Mehdi Mammadov on 14.06.2017.
 */

function openDialog() {
    showDialog();
    return false;
}

function showDialog() {
    var dID = "createPlaylistDialog";
    var checkDialog = document.getElementById(dID);
    if(checkDialog){ return false; }

    var modalCreateDialog = document.createElement("div");
    modalCreateDialog.setAttribute("id", dID);
    var geom = {};
    geom.x = ($(window).width() - 520) / 2;
    geom.y = ($(window).height() - 240) / 2;
    geom.styleString = "left:" + geom.x + "px;top:" + geom.y + "px;";

    modalCreateDialog.setAttribute("style", geom.styleString);

    var dHeader = document.createElement("div");
    dHeader.setAttribute("id", "cdHeader");

    var dLabel = document.createElement("div");
    dLabel.setAttribute("id", "cdLabel");
    dLabel.appendChild(document.createTextNode("Create new playlist"));
    dHeader.appendChild(dLabel);


    // Dialog body
    var dBodyDiv = document.createElement("div");
    dBodyDiv.setAttribute("id", "cdBody");

    var dInputLabel = document.createElement("div");
    dInputLabel.setAttribute("id", "cdInputLabel");
    dInputLabel.appendChild(document.createTextNode("input playlist file name:"));
    dBodyDiv.appendChild(dInputLabel);

    var dInputFieldDiv = document.createElement("div");
    dInputFieldDiv.setAttribute("id", "cdInputFieldDiv");

    var dInputField = document.createElement("input");
    dInputField.setAttribute("id", "cdInputField");
    dInputField.setAttribute("type", "text");
    dInputFieldDiv.appendChild(dInputField);
    dBodyDiv.appendChild(dInputFieldDiv);


    // Dialog button bar
    var dButtonBar = document.createElement("div");
    dButtonBar.setAttribute("id", "cdButtonBar");

    var documentUri = window.location.pathname;
    var aCreateButton = document.createElement("a");
    aCreateButton.setAttribute("id", "aCreateNewPlaylist");
    aCreateButton.setAttribute("href", window.location);
    aCreateButton.setAttribute("onclick", ("createNewPlaylist()"));

    var dCreateButton = document.createElement("div");
    dCreateButton.setAttribute("id", "cdCreateButton");
    dCreateButton.appendChild(document.createTextNode("CREATE"));
    aCreateButton.appendChild(dCreateButton);

    dButtonBar.appendChild(aCreateButton);

    var aCancelButton = document.createElement("a");
    aCancelButton.setAttribute("href", "#");
    aCancelButton.setAttribute("onclick", ("closeDialog(\"" + dID + "\")"));

    var dCancelButton = document.createElement("div");
    dCancelButton.setAttribute("id", "cdCancelButton");
    dCancelButton.appendChild(document.createTextNode("CANCEL"));
    aCancelButton.appendChild(dCancelButton);

    dButtonBar.appendChild(aCancelButton);

    modalCreateDialog.appendChild(dHeader);
    modalCreateDialog.appendChild(dBodyDiv);
    modalCreateDialog.appendChild(dButtonBar);

    var bodyOfPage = document.getElementById("pageHeader");
    bodyOfPage.appendChild(modalCreateDialog);
}

function createNewPlaylist() {
    var newPlaylistFilename = document.getElementById("cdInputField").value;
    var documentUri = window.location.pathname;

    var dID = "createPlaylistDialog";

    $.ajax({
        url: documentUri + '/new',
        data: {
            'playlistFilename' : newPlaylistFilename
        },
        cache: false,
        type: "GET"
    });

    closeDialog(dID);

}