/**
 * Created by mehdi on 17.06.2017.
 */

function loadDynamicData() {
    var currentLocation = window.location.pathname;
    switch (currentLocation){
        case '/list':
            addExistedPlaylists();
            break;
        case '/edit':
            addExistingFiles();
            addOptionsToComboboxAtEdit();
            break;
        default:
            break;
    }
}

function addExistedPlaylists() {
    playlistsJSONtext = playlistsJSONtext.replace(/(&quot\;)/g, "\"");

    var playlistsObj = JSON.parse(playlistsJSONtext);
    var pListViewItem = document.getElementById("playlistsListView");

    playlistsObj.sort(function (a, b) {
        if(a.id < b.id)
            return -1;
        else if(a.id > b.id)
            return 1;
        else 0;
    });

    for (var u = 0; u < playlistsObj.length; u++) {
        var newListViewItem = document.createElement("div");
        newListViewItem.className = "listviewItem";

        var newFilenameItem = document.createElement("div");
        newFilenameItem.className = "filename";
        newFilenameItem.appendChild(document.createTextNode(playlistsObj[u].id + ".smil"));
        newListViewItem.appendChild(newFilenameItem);

        var newFileDateItem = document.createElement("div");
        newFileDateItem.className = "fileDate";
        newFileDateItem.appendChild(document.createTextNode(playlistsObj[u].href));
        newListViewItem.appendChild(newFileDateItem);

        pListViewItem.appendChild(newListViewItem);
    }

    proceedMessage(messageObject);
}

function addOptionsToComboboxAtEdit() {
    var cBoxObject = document.getElementById("selectPlaylistComboBox");
    var defaultOption = document.createElement("option");
    defaultOption.value = undefined;
    defaultOption.setAttribute("selected", "selected");
    defaultOption.className = "selectPlaylistOption";

    if(playlistsJSONtext !== undefined){
        defaultOption.text = "Select playlist ...";
        cBoxObject.appendChild(defaultOption);
        playlistsJSONtext = playlistsJSONtext.replace(/(&quot\;)/g, "\"");
        var optionObjects = JSON.parse(playlistsJSONtext);

        optionObjects.sort(function (a, b) {
            if(a.id < b.id)
                return -1;
            else if(a.id > b.id)
                return 1;
            else 0;
        });

        for(var oCounter = 0; oCounter < optionObjects.length; oCounter++){
            var newOption = document.createElement("option");
            newOption.className = "selectPlaylistOption";
            newOption.value = optionObjects[oCounter].id;
            newOption.text = optionObjects[oCounter].id + ".smil";
            cBoxObject.appendChild(newOption);
        }
    }
    else {
        defaultOption.text = "Server is empty...";
        cBoxObject.appendChild(defaultOption);
        cBoxObject.disabled = true;
    }
}

function addExistingFiles() {
    existedMediaListAsString = existedMediaListAsString.replace(/(&quot\;)/g, "\"");

    mediaListJsonObject = JSON.parse(existedMediaListAsString);

    var ul = document.getElementById("existedMediaFiles");
    ul.innerHTML = "";

    for (var i = 0; i < mediaListJsonObject.length; i++) {
        var listItem = document.createElement("li");
        listItem.className = "ui-widget-content";


        var liContainer = document.createElement("div");
        liContainer.className = "li-container";

        var liFilename = document.createElement("div");
        liFilename.className = "filename";
        liFilename.appendChild(document.createTextNode(mediaListJsonObject[i].mediaFilename));
        liContainer.appendChild(liFilename);

        var liFileinfo = document.createElement("div");
        liFileinfo.className = "fileInfo";

        var liHBLabel = document.createElement("div");
        liHBLabel.className = "hasBitrateVariantLabel";
        liHBLabel.appendChild(document.createTextNode("available on"));
        liFileinfo.appendChild(liHBLabel);

        var liHB = document.createElement("div");
        liHB.className = "hasBitrateVariant";

        var liHBText = "";

        for(var a = 0; a < mediaListJsonObject[i].bitrateVariants.length; a++){
            liHBText += mediaListJsonObject[i].bitrateVariants[a];
            if(a < mediaListJsonObject[i].bitrateVariants.length - 1){
                liHBText += ", ";
            }
        }

        liHB.appendChild(document.createTextNode(liHBText));
        liFileinfo.appendChild(liHB);

        var liHPLabel = document.createElement("div");
        liHPLabel.className = "hasProblemsLabel";
        liHPLabel.appendChild(document.createTextNode("problems"));
        liFileinfo.appendChild(liHPLabel);

        var liHP = document.createElement("div");

        if(mediaListJsonObject[i].bitrateVariants.length < bitrateVariantsCount){
            liHP.appendChild(document.createTextNode(bitrateVariantsCount - mediaListJsonObject[i].bitrateVariants.length));
            liHP.className = "hasProblems";
        }
        else {
            liHP.appendChild(document.createTextNode("0"));
            liHP.className = "hasnotProblems";
        }

        liFileinfo.appendChild(liHP);

        liContainer.appendChild(liFileinfo);

        listItem.appendChild(liContainer);
        ul.appendChild(listItem);
    }
}