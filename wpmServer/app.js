var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var eSession = require('express-session');
var eRedisStore = require('connect-redis')(eSession);
var eSecretKey = 'fa7f3a6a-4dff-11e7-b114-b2f933d5fe66';

var indexPage = require('./routes/index');
var listPage = require('./routes/list');
var editPage = require('./routes/edit');
//var createPlaylistPage = require('./routes/createPlaylist');
//var delPage = require('./routes/delete');
var loadPlaylistPage = require('./routes/loadPlaylist');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, 'public', 'img/favicon.ico')));
//app.use(logger('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(eSession({
    resave: false,
    saveUninitialized: false,
    secret: eSecretKey,
    store: new eRedisStore
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', indexPage);
app.use('/list', listPage);
app.use('/edit', editPage);
//app.use('/create', createPlaylistPage);
//app.use('/del', delPage);
app.use('/load', loadPlaylistPage);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
