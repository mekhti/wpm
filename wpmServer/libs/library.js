/**
 * Created by mehdi on 10.06.2017.
 */

var express = require('express');
var router = express.Router();
var fileHandler = require('fs');

module.exports.getAppConfiguration = function (confFilename) {

    if(confFilename === undefined){
        confFilename = 'conf/wpm.json';
    }

    var fStatistics = fileHandler.statSync(confFilename);
    var data = fileHandler.readFileSync(configFilename);
    return JSON.parse(data);
}