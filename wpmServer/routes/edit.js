/**
 * Created by Mehdi Mammadov on 11.06.2017.
 */

var express = require('express');
var fileHandler = require('fs');
var router = express.Router();
var flash = require('connect-flash');

var restClient = require('request');
var waitFor = require('p-wait-for');

var ssh2Clent = require('ssh2').Client;
var configObject = {};
var playlists = [];
var mediaFilesListGlob = [];
var bitrateVariantsCount = 0;
var filesContentAsArray = [];
var messageObject = {};

var semaphore = false;

var loopSemaphore = {};
loopSemaphore.counter = 0;
loopSemaphore.semaphore = false;

var defaultTimeout = 1;

router.use(flash());

router.get('/', function (req, res, next) {
    if(req.session.isAutorized){
        preparePageData();

        waitFor(function () {
            if(mediaFilesListGlob.length > 0 && playlists.length > 0)
                return true;
            else return false;
        }, defaultTimeout).then(
            function () {
                res.render( 'edit', {
                    flash: req.flash(),
                    existedMediaList: JSON.stringify( mediaFilesListGlob ),
                    bitrateVariantsCount: bitrateVariantsCount,
                    playlists: JSON.stringify( playlists ),
                    messageObject: JSON.stringify( messageObject )
                });
            });
    }
    else {
        res.redirect('/');
    }
});

router.get('/new', function (req, res, next) {
    var semaphore = false;

    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles/";
    reqURI += req.query.playlistFilename;

    restClient({
        url: reqURI,
        method: 'POST',
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        },
        encoding: 'utf8',
        json: true,
        body: {
            "restURI": reqURI,
            "smilStreams": []
        },
        auth: {
            username: configObject.WowzaSettings.AdminUsername,
            password: configObject.WowzaSettings.AdminPassword,
            sendImmediately: false
        }
    }, function (postReqErr, response, body) {
        if(postReqErr)
            console.log(postReqErr);

        waitFor(function () {
            if(body.length <= 5){
                return false;
            }
            else {
                return true;
            }
        }, defaultTimeout).then(
            function () {
                if(body.success === true){
                    messageObject.isErrorAccured = false;
                    messageObject.messageText = body.message;
                }
                else {
                    messageObject.isErrorAccured = true;
                    messageObject.messageText = "FATAL ERROR! Message from Wowza server: "
                        + body.message;
                }
                semaphore = true;
            });
    });

    waitFor(function () {
        if(semaphore === true)
            return true;
        else return false
    }, defaultTimeout).then(function () {
        res.redirect('/edit');
    });

});

router.get('/del', function (req, res, next) {
    var semaphore = false;

    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles/";
    reqURI += req.query.delPlaylistFilename;

    console.log("URL is: " + reqURI);

    restClient({
        url: reqURI,
        method: 'DELETE',
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        },
        encoding: 'utf8',
        auth: {
            username: configObject.WowzaSettings.AdminUsername,
            password: configObject.WowzaSettings.AdminPassword,
            sendImmediately: false
        }
    }, function (delReqErr, response, body) {
        if(delReqErr)
            console.log(delReqErr);
        console.log("Body of response: " + body);

        waitFor(function () {
            if(body.length <= 5){
                return false;
            }
            else {
                return true;
            }
        }, defaultTimeout).then(
            function () {
                if(body.success === true){
                    messageObject.isErrorAccured = false;
                    messageObject.messageText = body.message;
                }
                else {
                    messageObject.isErrorAccured = true;
                    messageObject.messageText = "FATAL ERROR! Message from Wowza server: "
                        + body.message;
                }
                semaphore = true;
            });
    });

    waitFor(function () {
        if(semaphore === true)
            return true;
        else return false;
    }, defaultTimeout).then(function () {
        res.redirect('/edit');
    });

});

function handleSemaphore() {
    if(loopSemaphore.counter === 0){
        loopSemaphore.semaphore = false;
    }
    else {
        loopSemaphore.semaphore = true;
    }

    return loopSemaphore.semaphore;
}

function getAppConfiguration(configFilename) {
    if(configFilename === undefined)
        configFilename = 'conf/wpm.json';

    var fStatistics = fileHandler.statSync(configFilename);
    if(!!fStatistics.isFile){
        var data = fileHandler.readFileSync(configFilename);
        return JSON.parse(data);
    }
    else{
        return JSON.parse('{}');
    }
}

function preparePageData() {
    playlists.length = 0;
    configObject = getAppConfiguration();
    getSmilList();

    if(mediaFilesListGlob.length > 0){
        mediaFilesListGlob.length = 0;
    }

    var folderPath, variantId, tmpFilename;
    bitrateVariantsCount = configObject.PlaylistSettings.bitrateVariants.length;

    for(var i = 0; i < bitrateVariantsCount; i++){

        folderPath = configObject.WowzaSettings.MediaStorageBasePath +
            configObject.PlaylistSettings.bitrateVariants[i].subdir;
        variantId = configObject.PlaylistSettings.bitrateVariants[i].id;
        tmpFilename = configObject.TempFilePath + variantId + ".json";

        dumpObjectToFile(folderPath, variantId, tmpFilename);
        readObjectDataFromFile(tmpFilename, variantId);
    }

    parseAllDataFromRemote();
}

function getSmilList() {
    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles";

    var requestOptions = {
        url: reqURI,
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        }
    };

    restClient.get(requestOptions, function (error, response, body) {
        if(error)
            console.log(error);

        fillPlaylists(body);
        body = undefined;

    }).auth(
        configObject.WowzaSettings.AdminUsername,
        configObject.WowzaSettings.AdminPassword,
        false
    );
}

function fillPlaylists(jsonAsText) {
    var jsonObj = JSON.parse(jsonAsText);

    var smilID = ""; var smilHREF = "";

    for(var i = 0; i < jsonObj.smilFiles.length; i++){
        smilID = jsonObj.smilFiles[i].id;
        smilHREF = jsonObj.smilFiles[i].href;
        var newElement = {};
        newElement.id = smilID;
        newElement.href = smilHREF;

        playlists.push(newElement);
    }
}

function dumpObjectToFile(folderPath, variantId, tmpFile) {
    var ssh2Conn = new ssh2Clent();

    var ss2Settings = {
        host: configObject.SSH2Settings.hostAddress,
        port: configObject.SSH2Settings.port,
        username: configObject.SSH2Settings.username,
        password: configObject.SSH2Settings.password
    };

    ssh2Conn.on('ready', function () {
        ssh2Conn.sftp(function (connectionError, sftp) {
            if(connectionError)
                throw connectionError;

            sftp.readdir(folderPath, function (readdirError, list) {
                if(readdirError)
                    throw readdirError;

                do{
                    setTimeout(function () {}, defaultTimeout);
                } while (list === undefined);

                fileHandler.writeFile(tmpFile,
                    JSON.stringify(list), 'utf8', function (fileWritingError) {
                        if(fileWritingError)
                            throw fileWritingError;
                    });
            });
        });
    }).connect(ss2Settings);

    do{
        setTimeout(function () {}, defaultTimeout);
    } while (fileHandler.statSync(tmpFile).isFile === undefined);

    ssh2Conn.end();
}

function readObjectDataFromFile(tmpFile, variantId) {
    do{
        setTimeout(function () {}, defaultTimeout);
    } while (fileHandler.statSync(tmpFile).isFile === undefined);

    var fileContent = {};
    fileContent.variantId = variantId;
    fileContent.contentAsText = fileHandler.readFileSync(tmpFile);

    do{
        setTimeout(function () {}, defaultTimeout);
    } while (semaphore === true);

    semaphore = true;
    filesContentAsArray.push(fileContent);
    semaphore = false;
}

function parseAllDataFromRemote() {
    do{
        setTimeout(function () {}, defaultTimeout);
    } while (filesContentAsArray.length < bitrateVariantsCount);

    for(var j = 0; j < bitrateVariantsCount; j++){

        semaphore = true;

        if(loopSemaphore.semaphore === false){
            var isLastLoopFinished = fillMetadata(filesContentAsArray[j]);
        }

        do{
            setTimeout(function () {}, defaultTimeout);
        } while (handleSemaphore() === true);

        semaphore = false;
    }
}

function fillMetadata(dataObject) {

    loopSemaphore.counter++;

    var fileInfoObj = JSON.parse(dataObject.contentAsText);

    for( var i = 1; i <= fileInfoObj.length; i++ ){
        var isFileExists = false;

        var newElement = {};
        newElement.bitrateVariants = [];

        for( var j = 0; j < mediaFilesListGlob.length; j++){

            if(fileInfoObj[i-1].filename === mediaFilesListGlob[j].mediaFilename){
                mediaFilesListGlob[j].bitrateVariants.push(dataObject.variantId);
                isFileExists = true;
            }
        }

        if(isFileExists === false){
            newElement.mediaFilename = fileInfoObj[i-1].filename;
            newElement.bitrateVariants.push(dataObject.variantId);
            mediaFilesListGlob.push(newElement);
            continue;
        }
    }

    semaphore = false;

    loopSemaphore.counter--;

    return true;
}


module.exports = router;