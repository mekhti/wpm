var express = require('express');
var fileHandler = require('fs');
var router = express.Router();
var flash = require('connect-flash');

router.use(flash());

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render( 'index', { flash: req.flash() } );
});

router.post('/', function (req, res, next) {
      // console.log(req.body.username);
      // console.log(req.body.pass);
    var appConfig = getAppConfiguration();
      // console.log(appConfig)
    req.session.isAutorized = false;

    for (var i = 0; i < appConfig.Users.length; i++){
        if(appConfig.Users[i].username === req.body.username
            && appConfig.Users[i].password === req.body.pass){
            req.session.isAutorized = true;
        }
    }

    if(req.session.isAutorized){
        res.redirect('/list');
    }
    else {
        res.redirect('/');
    }

})


function getAppConfiguration(configFilename) {
    if(configFilename === undefined)
        configFilename = 'conf/wpm.json';

    var fStatistics = fileHandler.statSync(configFilename);
    if(!!fStatistics.isFile){
        var data = fileHandler.readFileSync(configFilename);
        return JSON.parse(data);
    }
    else{
        return JSON.parse('{}');
    }
}

module.exports = router;
