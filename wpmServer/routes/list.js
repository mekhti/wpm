/**
 * Created by mehdi on 11.06.2017.
 */

var express = require('express');
var fileHandler = require('fs');
var router = express.Router();
var flash = require('connect-flash');

var restClient = require('request');
var waitFor = require('p-wait-for');

var configObject = {};
var playlists = [];
var messageObject = {};

var defaultTimeout = 1;

router.use(flash());

router.get('/', function (req, res, next) {
    messageObject.isErrorAccured = false;
    messageObject.messageText = "";
    if(req.session.isAutorized){
        preparePageData();

        waitFor(function () {
            if(playlists.length > 0)
                return true;
            else return false;
        }, defaultTimeout).then(
            function () {
                res.render( 'list', {
                    flash: req.flash(),
                    playlists: JSON.stringify( playlists ),
                    messageObject: JSON.stringify( messageObject )
                });
            }
        );
    }
    else {
        res.redirect('/');
    }
});

router.get('/new', function (req, res, next) {
    var semaphore = false;

    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles/";
    reqURI += req.query.playlistFilename;

    restClient({
        url: reqURI,
        method: 'POST',
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        },
        encoding: 'utf8',
        json: true,
        body: {
            "restURI": reqURI,
            "smilStreams": []
        },
        auth: {
            username: configObject.WowzaSettings.AdminUsername,
            password: configObject.WowzaSettings.AdminPassword,
            sendImmediately: false
        }
    }, function (postReqErr, response, body) {
            if(postReqErr)
                console.log(postReqErr);

            waitFor(function () {
                if(body.length <= 5){
                    return false;
                }
                else {
                    return true;
                }
            }, defaultTimeout).then(
                function () {
                    if(body.success === true){
                        messageObject.isErrorAccured = false;
                        messageObject.messageText = body.message;
                    }
                    else {
                        messageObject.isErrorAccured = true;
                        messageObject.messageText = "FATAL ERROR! Message from Wowza server: "
                            + body.message;
                    }
                    semaphore = true;
                });
    });

    waitFor(function () {
        if(semaphore === true)
            return true;
        else return false
    }, defaultTimeout).then(function () {
        res.redirect('/list');
    });

});

router.get('/del', function (req, res, next) {
    var semaphore = false;

    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles/";
    reqURI += req.query.delPlaylistFilename;

    console.log("URL is: " + reqURI);

    restClient({
        url: reqURI,
        method: 'DELETE',
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        },
        encoding: 'utf8',
        auth: {
            username: configObject.WowzaSettings.AdminUsername,
            password: configObject.WowzaSettings.AdminPassword,
            sendImmediately: false
        }
    }, function (delReqErr, response, body) {
        if(delReqErr)
            console.log(delReqErr);
        console.log("Body of response: " + body);

        waitFor(function () {
            if(body.length <= 5){
                return false;
            }
            else {
                return true;
            }
        }, defaultTimeout).then(
            function () {
                if(body.success === true){
                    messageObject.isErrorAccured = false;
                    messageObject.messageText = body.message;
                }
                else {
                    messageObject.isErrorAccured = true;
                    messageObject.messageText = "FATAL ERROR! Message from Wowza server: "
                        + body.message;
                }
                semaphore = true;
            });
    });

    waitFor(function () {
        if(semaphore === true)
            return true;
        else return false;
    }, defaultTimeout).then(function () {
        res.redirect('/list');
    });

});

function preparePageData() {
    playlists.length = 0;
    configObject = getAppConfiguration();
    getSmilList();
}

function getSmilList() {
    var reqURI = configObject.WowzaSettings.APISettings.Protocol + "://";
    reqURI += configObject.WowzaSettings.APISettings.APIAddress + ":";
    reqURI += configObject.WowzaSettings.APISettings.APIPort + "/v2/servers/";
    reqURI += configObject.WowzaSettings.APISettings.ServerName + "/vhosts/";
    reqURI += configObject.WowzaSettings.APISettings.VHost + "/smilfiles";

    var requestOptions = {
        url: reqURI,
        headers: {
            'Accept' : 'application/json; charset=utf-8',
            'Content-Type' : 'application/json; charset=utf-8'
        }
    };

    restClient.get(requestOptions, function (error, response, body) {
        if(error)
            console.log(error);

        fillPlaylists(body);
        body = undefined;

    }).auth(
        configObject.WowzaSettings.AdminUsername,
        configObject.WowzaSettings.AdminPassword,
        false
    );
}

function fillPlaylists(jsonAsText) {
    var jsonObj = JSON.parse(jsonAsText);

    var smilID = ""; var smilHREF = "";

    for(var i = 0; i < jsonObj.smilFiles.length; i++){
        smilID = jsonObj.smilFiles[i].id;
        smilHREF = jsonObj.smilFiles[i].href;
        var newElement = {};
        newElement.id = smilID;
        newElement.href = smilHREF;

        playlists.push(newElement);
    }
}

function getAppConfiguration(configFilename) {
    if(configFilename === undefined)
        configFilename = 'conf/wpm.json';

    var fStatistics = fileHandler.statSync(configFilename);
    if(!!fStatistics.isFile){
        var data = fileHandler.readFileSync(configFilename);
        return JSON.parse(data);
    }
    else{
        return JSON.parse('{}');
    }
}

module.exports = router;