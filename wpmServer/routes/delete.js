/**
 * Created by Mehdi Mammadov on 17.06.2017.
 */

var express = require('express');
var fileHandler = require('fs');
var router = express.Router();
var flash = require('connect-flash');

var restClient = require('request');
var waitFor = require('p-wait-for');

var configObject = {};
var playlists = [];
var mediaFilesListGlob = [];
var bitrateVariantsCount = 0;
var filesContentAsArray = [];
var messageObject = {};

var defaultTimeout = 1;

router.use(flash());

router.get('/', function (req, res, next) {
    if(req.session.isAutorized){

    }
    else {
        res.redirect('/');
    }
});

module.exports = router;